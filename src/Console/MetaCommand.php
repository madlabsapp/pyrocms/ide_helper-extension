<?php

namespace Newebtime\IdeHelperExtension\Console;

class MetaCommand extends \Barryvdh\LaravelIdeHelper\Console\MetaCommand
{
    /**
     * Register an autoloader the throws exceptions when a class is not found.
     */
    protected function registerClassAutoloadExceptions()
    {
        spl_autoload_register(function ($class) {
            if (strpos($class, 'Anomaly') !== 0) {
                throw new \Exception("Class '$class' not found.");
            }
        });
    }
}
