<?php

namespace Newebtime\IdeHelperExtension;

use Anomaly\Streams\Platform\Addon\Extension\Extension;

class IdeHelperExtension extends Extension
{
    /**
     * @var null|string
     */
    protected $provides = null;
}
