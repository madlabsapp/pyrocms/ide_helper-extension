<?php

namespace Newebtime\IdeHelperExtension;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\Engines\PhpEngine;
use Illuminate\View\Factory;
use Illuminate\View\FileViewFinder;
use Newebtime\IdeHelperExtension\Console\MetaCommand;

class IdeHelperExtensionServiceProvider extends AddonServiceProvider
{
    /**
     * Register the addon.
     */
    public function register()
    {
        $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);

        $this->app->singleton(
            'command.ide-helper.meta',
            function ($app) {
                return new MetaCommand($app['files'], $this->createLocalViewFactory(), $app['config']);
            }
        );
    }

    /**
     * @return Factory
     */
    private function createLocalViewFactory()
    {
        $resolver = new EngineResolver();
        $resolver->register('php', function () {
            return new PhpEngine();
        });

        $finder  = new FileViewFinder($this->app['files'], [
            $this->app->make('path.base') . '/vendor/barryvdh/laravel-ide-helper/resources/views'
        ]);
        $factory = new Factory($resolver, $finder, $this->app['events']);
        $factory->addExtension('php', 'php');

        return $factory;
    }
}
